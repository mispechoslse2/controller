################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CFG_SRCS += \
../spiloopback.cfg 

CMD_SRCS += \
../MSP_EXP432P401R.cmd 

C_SRCS += \
../BSP.c \
../MSP_EXP432P401R.c \
../spiloopback.c 

GEN_CMDS += \
./configPkg/linker.cmd 

GEN_FILES += \
./configPkg/linker.cmd \
./configPkg/compiler.opt 

GEN_MISC_DIRS += \
./configPkg/ 

C_DEPS += \
./BSP.d \
./MSP_EXP432P401R.d \
./spiloopback.d 

GEN_OPTS += \
./configPkg/compiler.opt 

OBJS += \
./BSP.obj \
./MSP_EXP432P401R.obj \
./spiloopback.obj 

GEN_MISC_DIRS__QUOTED += \
"configPkg\" 

OBJS__QUOTED += \
"BSP.obj" \
"MSP_EXP432P401R.obj" \
"spiloopback.obj" 

C_DEPS__QUOTED += \
"BSP.d" \
"MSP_EXP432P401R.d" \
"spiloopback.d" 

GEN_FILES__QUOTED += \
"configPkg\linker.cmd" \
"configPkg\compiler.opt" 

C_SRCS__QUOTED += \
"../BSP.c" \
"../MSP_EXP432P401R.c" \
"../spiloopback.c" 


