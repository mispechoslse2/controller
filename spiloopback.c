/* XDCtools Header files */
#include <bateria40.h>
#include <xdc/std.h>
#include <xdc/runtime/System.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <BSP.h>
/* TI-RTOS Header files */
#include <ti/drivers/GPIO.h>
#include <ti/drivers/SPI.h>
/* Driver Header files */
#include <ti/drivers/ADC.h>
/* Example/Board Header files */
// Includes de totes les dependencies
#include "Board.h"
#include "msp432p401r.h"
#include "../inc/BSP.h"
#include "bateria40.h"
#include "bateria20.h"
#include "bateria60.h"
#include "bateria80.h"
#include "mark.h"
#include "off.h"
#include "on.h"
#include "INTRO.h"
#include "bateria100.h"
#include "escalaxy.h"
#include "escalaxy1.h"
#include "nobateria.h"
//-------------------//
#define SPI_MSG_LENGTH    26              // Definim la longitud del mistge trasm�s per SPI
#define TASKSTACKSIZE     768             // TaskStackSize
#define NUMRANDCOLORS 12                  // length of random color array
#define SLOPE 1664525                     // variables per a fer numeros aleatoris per a lcd
#define OFFSET 1013904223                 // variables per a fer numeros aleatoris per a lcd
#define SCREENCHARCOLS 26                 // total number of character columns in the screen
#define SCREENCHARROWS 13                 // total number of character rows in the screen
#define BUTTON1   (*((volatile uint8_t *)(0x42000000+32*0x4C40+4*1)))  /* Port 5.1 Input */
#define BUTTON2   (*((volatile uint8_t *)(0x42000000+32*0x4C20+4*5)))  /* Port 3.5 Input */
//-------------------//
Task_Struct task0Struct, task1Struct;
Char task0Stack[TASKSTACKSIZE], task1Stack[TASKSTACKSIZE];
//-------------------//
//inicializaci�n de nuestras variables

//uint16_t boto;
uint16_t x1, y1; uint8_t select1;         // joystick used to seed random number
uint16_t RevealScreen[SCREENCHARCOLS];    // set a bit in this array to reveal the character at corresponding row; LSB = row0
uint16_t adcValue0;                       // Variable per a llegir el valor de el joystick del mando
uint16_t adcValue1;                       // Variable per a llegir el valor de el joystick del mando
int f;          //variable per fer un for
int cerox;      //variable cero per calibrar el estatus del joystick per la cordenada x
int ceroy;      //variable cero per calibrar el estatus del joystick per la cordenada y
unsigned char masterRxBuffer[SPI_MSG_LENGTH];      //variables per a crear els buffers de SPI
unsigned char masterTxBuffer[SPI_MSG_LENGTH] = "Hello, this is master SPI";
unsigned char slaveRxBuffer[SPI_MSG_LENGTH];       //variables per a crear els buffers de SPI
unsigned char slaveTxBuffer[SPI_MSG_LENGTH] =  "Hello, this is slave SPI";
const uint16_t RandomColors[NUMRANDCOLORS] = { // random colors seguint el codi RGB
  0x801F,  // 255, 0, 128
  0x001F,  // 255, 0, 0
  0x041F,  // 255, 128, 0
  0x07FF,  // 255, 255, 0
  0x07F0,  // 128, 255, 0
  0x07E0,  // 0, 255, 0
  0x87E0,  // 0, 255, 128
  0xFFE0,  // 0, 255, 255
  0xFC00,  // 0, 128, 255
  0xF800,  // 0, 0, 255
  0xF810,  // 128, 0, 255
  0xF81F,  // 255, 0, 255
};
//-------------------//
char ascii_numbers[10];     //variables per a printejar per lcd linies de informaci�
char ascii_numbers1[10];    //variables per a printejar per lcd linies de informaci�
char angulo[15];            //variable per a printejar per lcd linies de informaci�
char angulo1[15];           //variable per a printejar per lcd linies de informaci�
char boto[10];
char boto1[10];
/*variables per a definir els angles de joystick per analog input*/
int cordx;
int cordy;
int anglex;
int angley;
/*---------------------------------------------------------------*/
//Funcions per escriure en pantalla//
static uint32_t M=1;
uint32_t Random(void){    // 0 to 255, very random
  M = SLOPE*M+OFFSET;     // 1664525*M+1013904223
  return (M>>24);
}
uint32_t Random16(void){  // 0 to 65535, sort of random
  M = SLOPE*M+OFFSET;     // 1664525*M+1013904223
  return (M>>16);
}
//-------------------//
void revealchar(uint16_t x, uint16_t y, char *pt, int16_t textColor, int16_t falseColor, uint16_t revealChance){
  if((x >= SCREENCHARCOLS) || (y >= SCREENCHARROWS)){
    return;
  }
  if(RevealScreen[x]&(1<<y)){
    // character is revealed
    BSP_LCD_DrawChar(x*6, y*10, *pt, textColor, LCD_BLACK, 1);
  } else{
    // character is hidden
    if(Random16() < revealChance){
      // reveal the character
      RevealScreen[x] |= (1<<y);
      BSP_LCD_DrawChar(x*6, y*10, *pt, textColor, LCD_BLACK, 1);
    } else{
      // character is still hidden
      BSP_LCD_DrawChar(x*6, y*10, ('0' + (Random16()%75)), falseColor, LCD_BLACK, 1);
    }
  }
}
uint32_t revealstring(uint16_t y, char *pt, int16_t textColor){
  uint32_t count = 0;
  uint16_t x = 0;
  char *c = pt;
  while(*c){
    x = x + 1;
    c = c + 1;
  }
  x = 0; //13 - x/2;
  if(y>12) return 0;
  while(*pt){
    revealchar(x, y, pt, textColor, RandomColors[Random()%NUMRANDCOLORS], 6554);
    pt++;
    x = x+1;
    if(x>25) return count;  // number of characters printed
    count++;
  }
  return count;  // number of characters printed
}
//Tasca ADC//
Void taskFxn0(UArg arg0)
{
    ADC_Handle   adc;
    ADC_Handle   adc1;
    ADC_Params   params;
    while(1){
        ADC_Params_init(&params);               //inicialitzem ADC per a la lectura de un dels potenciometres del joystick
        adc1 = ADC_open(Board_ADC15, &params);  //cridem amb el handle el adc15 i utilitzem els ADC_params "params"
        ADC_convert(adc1, &adcValue0);          //realitzem la conversi� anal�gica digital al port 15 i depositem el valor a la variable adcValue0
        ADC_close(adc1);                        //tanquem ADC per iniciar la seg�ent conversi�
        adc = ADC_open(Board_ADC9, &params);    //inicialitzem ADC per a la lectura de un dels potenciometres del joystick
        ADC_convert(adc, &adcValue1);           //realitzem la conversi� anal�gica digital al port 9 i depositem el valor a la variable adcValue1
        ADC_close(adc);                         //tanquem ADC

        /* El seguent codi, esta disenyat per a
         * obtindre valors de entre 30 i -30
         * graus que sera la angulatura m�xima
         * que podra adquirir el dron per a
         * realitzar les maniobres de gir
         */

        if (adcValue0>cerox){
            cordx = ((adcValue0-cerox)*100)/(16400-cerox);
            anglex = (cordx*30)/100;
            cordx = (cordx*32)/100;
            cordx = 32 - cordx;
            cordx  = cordx + 8;
        }
        if (adcValue0<=cerox){
            cordx = ((adcValue0)*100)/cerox;
            anglex = -(30-(cordx*30)/100);
            cordx = (cordx*32)/100;
            cordx = 32 - cordx;
            cordx  = 42 + cordx;
        }
        if (adcValue1>ceroy){
            cordy = ((adcValue1-ceroy)*100)/(16400-ceroy);
            angley = (cordy*30)/100;
            cordy = (cordy*32)/100;
            cordy = 32 - cordy;
            cordy  = cordy + 8;
        }
        if (adcValue1<=ceroy){
            cordy = ((adcValue1)*100)/ceroy;
            angley = -(30-(cordy*30)/100);
            cordy = (cordy*32)/100;
            cordy = 32 - cordy;
            cordy  = 42 + cordy;
        }
        /* El seg�ent codi esta dissenyat per a
         * convertir les variables "anglex" i
         * "angley" en ASCII i les ubiquem en les variables
         * ASCII numbers
         */
        sprintf(    ascii_numbers, "%d",    anglex );
        sprintf(    ascii_numbers1, "%d",   angley );
        sprintf(boto, "%d", BUTTON1);
        sprintf(boto1, "%d", BUTTON2);

        /*Aquest codi unicament fa la acci� de shifter entre els n�meros
         * que es printejaran al LCD amb la finalitat de que quan els valors
         * de sortida siguin de 2 o 1 d�git, no es printejin en 3 digits, sino
         * en els que han de ser         *
         */
        if (anglex<100){
            if (anglex<10){
                if(anglex<0){
                    if (anglex<-10){
                        ascii_numbers[2] = ascii_numbers[2];
                    }
                    else{
                        ascii_numbers[2] = ascii_numbers[1];
                        ascii_numbers[1] = ascii_numbers[0];
                        ascii_numbers[0] = 0x20; // Print de un valor en black (es un espai)
                    }
                }
                else{
                    ascii_numbers[2] = ascii_numbers[0];
                    ascii_numbers[0] = 0x20;
                    ascii_numbers[1] = 0x20;
                }
            }
            else{
                ascii_numbers[2] = ascii_numbers[1];
                ascii_numbers[1] = ascii_numbers[0];
                ascii_numbers[0] = 0x20;

            }
        }
        if (angley<100){
            if (angley<10){
                if(angley<0){
                    if (angley<-10){
                        ascii_numbers1[2] = ascii_numbers1[2];
                    }
                    else{
                        ascii_numbers1[2] = ascii_numbers1[1];
                        ascii_numbers1[1] = ascii_numbers1[0];
                        ascii_numbers1[0] = 0x20;
                    }
                }
                else{
                    ascii_numbers1[2] = ascii_numbers1[0];
                    ascii_numbers1[0] = 0x20;
                    ascii_numbers1[1] = 0x20;
                }
            }
            else{
                ascii_numbers1[2] = ascii_numbers1[1];
                ascii_numbers1[1] = ascii_numbers1[0];
                ascii_numbers1[0] = 0x20;

            }
        }

        /* Printeig en ASCII de Angle X:
         * seguit de ascii numbers, que cont� la informaci� del angle
         * desitjat provinguent de la lectura dels potenciometres del
         * joystick
         */

        angulo[0]=0x41;
        angulo[1]=0x6E;
        angulo[2]=0x67;
        angulo[3]=0x6C;
        angulo[4]=0x65;
        angulo[5]=0x20;
        angulo[6]=0x58;
        angulo[7]=0x3a;
        angulo[8]=0x20;
        angulo[9]=ascii_numbers[0];
        angulo[10]=ascii_numbers[1];
        angulo[11]=ascii_numbers[2];

        /* Printeig en ASCII de Angle Y:
         * seguit de ascii numbers, que cont� la informaci� del angle
         * desitjat provinguent de la lectura dels potenciometres del
         * joystick
         */

        angulo1[0]=0x41;
        angulo1[1]=0x6E;
        angulo1[2]=0x67;
        angulo1[3]=0x6C;
        angulo1[4]=0x65;
        angulo1[5]=0x20;
        angulo1[6]=0x59;
        angulo1[7]=0x3a;
        angulo1[8]=0x20;
        angulo1[9]=ascii_numbers1[0];
        angulo1[10]=ascii_numbers1[1];
        angulo1[11]=ascii_numbers1[2];

        // Si es pitja el bot� 1 Printeja la imatge "on" per LCD
        //ISR de enjegada del dron
        if (!BUTTON1){
            BSP_LCD_DrawBitmap(85,32,on,32,32);
        }
        // Si es pitja el bot� 2 es calibra el 0 de els angles del joystick amb la posici� actual del joystick

        if (!BUTTON2){ //HWI interrupt gpio joystick
            cerox = adcValue0;
            ceroy = adcValue1;
        }
        //Si es pitja el bot� 1 Printeja la imatge "off" per LCD
        //ISR per apagar el dron
        if (!(*((volatile uint8_t *)(0x42000000+32*0x4C21+4*1))) ){ //HWI interrupt gpio joystick
            BSP_LCD_DrawBitmap(85,32,off,32,32);
        }
        // Printeig de les barres de control del dron per LCD
        BSP_LCD_DrawBitmap(15,77,xiy,29,78);
        BSP_LCD_DrawBitmap(45,77,xiy1,29,78);
        BSP_LCD_DrawBitmap(34,cordx+1,mark,9,3);
        BSP_LCD_DrawBitmap(64,cordy+1,mark,9,3);
        for (f = 0; f<100000; f++){

        }
        // Printejem les variables de angulo i angulo1 per LCD
        revealstring(9, angulo, LCD_WHITE);
        revealstring(11,  angulo1, LCD_WHITE);
    }

}
Void slaveTaskFxn (UArg arg0)
{
    SPI_Handle slaveSpi;
    SPI_Params slaveSpiParams;
    SPI_Transaction slaveTransaction;
    bool transferOK;
    /* Initialize SPI handle with slave mode*/
    SPI_Params_init(&slaveSpiParams);
    slaveSpiParams.mode = SPI_SLAVE;
    slaveSpi = SPI_open(Board_SPI1, &slaveSpiParams);
    if (slaveSpi == NULL) {
        System_abort("Error initializing SPI\n");
    }
    else {
        System_printf("SPI initialized\n");
    }
    /* Initialize slave SPI transaction structure*/
    slaveTransaction.count = SPI_MSG_LENGTH;
    slaveTransaction.txBuf = (Ptr)slaveTxBuffer;
    slaveTransaction.rxBuf = (Ptr)slaveRxBuffer;
    /* Initiate SPI transfer*/
    transferOK = SPI_transfer(slaveSpi, &slaveTransaction);
    if(transferOK) {
        /* Print contents of slave receive buffer*/
        System_printf("Slave: %s\n", slaveRxBuffer);
    }
    else {
        System_printf("Unsuccessful slave SPI transfer");
    }
    /* Deinitialize SPI*/
    SPI_close(slaveSpi);
}

/*
 *  ======== masterTaskFxn ========
*/
Void masterTaskFxn (UArg arg0)
{
    SPI_Handle masterSpi;
    SPI_Transaction masterTransaction;
    bool transferOK;

    /* Initialize SPI handle as default master*/
    masterSpi = SPI_open(Board_SPI0, NULL);
    if (masterSpi == NULL) {
        System_abort("Error initializing SPI\n"); // error de inicializaci�n
    }
    else {
        System_printf("SPI initialized\n");
    }
    /* Initialize master SPI transaction structure*/
    masterTransaction.count = SPI_MSG_LENGTH;
    masterTransaction.txBuf = (Ptr)masterTxBuffer;
    masterTransaction.rxBuf = (Ptr)masterRxBuffer;
    /* Initiate SPI transfer*/
    transferOK = SPI_transfer(masterSpi, &masterTransaction);
    if(transferOK) {
        /* Print contents of master receive buffer*/
        System_printf("Master: %s\n", masterRxBuffer);
    }
    else {
        System_printf("Unsuccessful master SPI transfer");
    }
    SPI_close(masterSpi);
    System_printf("Done\n");
    System_flush();
}
/* variable to be read by GUI Composer */
int main(void)
 {
    Task_Params taskParams;
    Board_initGeneral();
    Board_initGPIO();
    Board_initSPI();
    Board_initADC();
    BSP_Joystick_Init();
    BSP_Joystick_Input(&x1,&y1,&select1);
    BSP_LCD_Init();
    BSP_Button1_Init();
    BSP_Button1_Input();
    BSP_Button2_Input();
    BSP_LCD_FillScreen(0x0000);
    BSP_LCD_DrawBitmap(85,32,off,32,32);
    BSP_LCD_DrawBitmap(85,110,intro,32,70);

    /* Construct master/slave Task threads
     * SPI
     * */
    Task_Params_init(&taskParams);

    taskParams.priority = 1;
    taskParams.stackSize = TASKSTACKSIZE;
    taskParams.stack = &task0Stack;
    Task_construct(&task0Struct, (Task_FuncPtr)masterTaskFxn, &taskParams, NULL);
    taskParams.stack = &task1Stack;
    taskParams.priority = 2;
    Task_construct(&task1Struct, (Task_FuncPtr)slaveTaskFxn, &taskParams, NULL);

    /* Create tasks
     * ADC
     */
    Task_Params_init(&taskParams);
    taskParams.priority = 10;
    taskParams.arg0 = 5000;
    Task_create(taskFxn0,&taskParams, NULL);

    /* Start BIOS */
    BIOS_start();

    return (0);
}
